import jenkins.model.*
import hudson.security.*

def instance = Jenkins.getInstance()

def hudsonRealm = new HudsonPrivateSecurityRealm(false)
def env = System.getenv()
hudsonRealm.createAccount("administrator", env['ADMIN_PASSWORD'])
instance.setSecurityRealm(hudsonRealm)

def strategy = new GlobalMatrixAuthorizationStrategy()
strategy.add(Jenkins.ADMINISTER, "administrator")
instance.setAuthorizationStrategy(strategy)

instance.save()